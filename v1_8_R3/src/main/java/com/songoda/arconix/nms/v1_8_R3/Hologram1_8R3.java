package com.songoda.arconix.nms.v1_8_R3;

import com.songoda.arconix.api.hologram.HologramObject;
import com.songoda.arconix.api.packets.Hologram;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.*;

public class Hologram1_8R3 implements Hologram {

    private Map<Location, HologramObject> registeredHolograms = new HashMap<>();

    public Hologram1_8R3() {
        reloadHologram();
    }

    private void reloadHologram() {
        for (HologramObject hologram : registeredHolograms.values()) {
            addHologram(hologram);
        }
    }

    @Override
    public HologramObject addHologram(HologramObject hologram) {
        if (hologram.getWorld() == null || hologram.getLocation() == null) return null;
        this.registeredHolograms.put(hologram.getLocation(), hologram);


        double increment = 0;

        for (String line : hologram.getLines()) {
            Location location = hologram.getLocation().subtract(0, increment, 0);

            Collection<org.bukkit.entity.Entity> nearbyEntite = location.getWorld().getNearbyEntities(location, 1, 1, 1);
            nearbyEntite.removeIf(e -> e.getType() != EntityType.ARMOR_STAND || e.getLocation().getX() != location.getX() || e.getLocation().getY() != location.getY() || e.getLocation().getZ() != location.getZ());

            EntityArmorStand nmsEntity = new EntityArmorStand(((CraftWorld) hologram.getWorld()).getHandle(), hologram.getX(), hologram.getY() - increment, hologram.getZ());

            if (nearbyEntite.size() == 0) {
                removeEntityFromWorld(location);
            } else {
                nearbyEntite.iterator().next().setCustomName(line);
                increment += .25;
                continue;
            }

            nmsEntity.setCustomName(line); // Only difference between 1.12 and 1.13
            nmsEntity.setCustomNameVisible(true);
            nmsEntity.setInvisible(true);
            nmsEntity.setGravity(true);
            nmsEntity.setSmall(true);

            NBTTagCompound compoundTag = new NBTTagCompound(); // 1.8R3 does not have the marker option but has it if you use NBT tags.
            nmsEntity.c(compoundTag);
            compoundTag.setBoolean("Marker", true);
            nmsEntity.f(compoundTag);

            nmsEntity.setBasePlate(true);

            WorldServer nmsWorld = ((CraftWorld) hologram.getWorld()).getHandle();

            this.setPosition(nmsEntity, location);

            if (nearbyEntite.size() == 0) {
                nmsWorld.addEntity(nmsEntity, CreatureSpawnEvent.SpawnReason.CUSTOM);
            }

            increment += .25;
        }

        return hologram;
    }

    public HologramObject removeHologram(HologramObject hologram) {
        if (hologram == null) return null;
        registeredHolograms.remove(hologram.getLocation());
        removeHologram(hologram, hologram.getLocation(), hologram.getLines().size());
        return hologram;
    }


    @Override
    public void removeHologram(Location location, int lines) {
        removeHologram(registeredHolograms.get(location), location, lines);
    }

    @Override
    public void updateVisible(HologramObject hologramObject, Location location, Player player, boolean visible) {

    }

    @Override
    public void removeHologram(HologramObject hologramObject, Location location, int lines) {
        if (location == null) return;

        double increment = 0;

        for (int i = 0; i < lines; i++) {
            Location location2 = location.clone().subtract(0, increment, 0);

            removeEntityFromWorld(location2);

            increment += .25;
        }
    }

    private void setPosition(Entity entity, Location location) {
        entity.setPosition(location.getX(), location.getY(), location.getZ());
        PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(entity);
        for (EntityHuman eh : entity.world.players) {
            if (!(eh instanceof EntityPlayer)) continue;
            EntityPlayer player = (EntityPlayer) eh;
            double distanceSquared = Math.pow(player.locX - entity.locX, 2) + Math.pow(player.locZ - entity.locZ, 2);
            if (distanceSquared < 8192 && player.playerConnection != null) {
                player.playerConnection.sendPacket(teleportPacket);
            }

        }
    }

    private void removeEntityFromWorld(Location location) {
        Collection<org.bukkit.entity.Entity> nearbyEntite = location.getWorld().getNearbyEntities(location, 2, 2, 2);
        for (org.bukkit.entity.Entity entity : nearbyEntite) {
            if (entity.getType() == EntityType.ARMOR_STAND) {
                if (location.getX() == entity.getLocation().getX() && location.getY() == entity.getLocation().getY() && location.getZ() == entity.getLocation().getZ()) {
                    entity.remove();
                }
            }
        }
    }

    public HologramObject getHologram(Location location) {
        return registeredHolograms.get(location);
    }

    @Override
    public HologramObject getHologram(String name) {
        for (HologramObject hologram : registeredHolograms.values()) {
            if (hologram.getName() != null && hologram.getName().equalsIgnoreCase(name))
                return hologram;
        }
        return null;
    }

    public List<HologramObject> getHolograms() {
        return new ArrayList<>(registeredHolograms.values());
    }

    @Deprecated
    public void spawnHolograms(Location location, List<String> holograms) {
        addHologram(new HologramObject(null, location, new ArrayList<>(holograms)));
    }

    @Deprecated
    public void spawnHologram(Location location, String line) {
        addHologram(new HologramObject(null, location, line));
    }

    @Deprecated
    public void despawnHologram(Location location) {
        removeHologram(location, 5);
    }

    @Override
    public void addHologram(Location location) {
        addHologram(new HologramObject(null, location, (String) null));

    }

    @Deprecated
    public ArrayList<Location> getLocations() {
        ArrayList<Location> locations = new ArrayList<>();
        for (HologramObject hologram : registeredHolograms.values()) {
            locations.add(hologram.getLocation());
        }
        return locations;
    }
}
