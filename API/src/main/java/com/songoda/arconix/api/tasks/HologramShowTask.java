package com.songoda.arconix.api.tasks;

import com.songoda.arconix.api.ArconixAPI;
import com.songoda.arconix.api.hologram.HologramObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class HologramShowTask extends BukkitRunnable {

    private static HologramShowTask instance;

    private final ArconixAPI api;

    public HologramShowTask(ArconixAPI plugin) {
        this.api = plugin;
    }

    public static HologramShowTask startTask(ArconixAPI plugin) {
        if (instance == null) {
            instance = new HologramShowTask(plugin);
            instance.runTaskTimer(Bukkit.getServer().getPluginManager().getPlugin("Arconix"), 0, 2);
        }

        return instance;
    }

    @Override
    public void run() {
        int distance = instance.api.plugin.getConfig().getInt("settings.Hologram-Distance");
        if (api.packetLibrary.getHologramManager() == null) return;
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (HologramObject hologramObject : api.packetLibrary.getHologramManager().getHolograms()) {
                if (hologramObject.getLocation() == null || hologramObject.getWorld() == null || hologramObject.getWorld() != player.getWorld()) continue;
                api.packetLibrary.getHologramManager().updateVisible(hologramObject, hologramObject.getLocation(), player, hologramObject.getLocation().distance(player.getLocation()) <= distance);
            }
        }
    }
}