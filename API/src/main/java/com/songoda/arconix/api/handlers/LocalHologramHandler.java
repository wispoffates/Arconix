package com.songoda.arconix.api.handlers;

import com.songoda.arconix.api.ArconixAPI;
import com.songoda.arconix.api.hologram.HologramObject;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by songoda on 4/2/2017.
 */
@SuppressWarnings("WeakerAccess")
public class LocalHologramHandler {
    private ArconixAPI api = ArconixAPI.getApi();

    public LocalHologramHandler() {
    }

    public List<HologramObject> getHologramList() {
        List<HologramObject> holograms = new ArrayList<>();
        if (api.hologramFile.getConfig().contains("Holograms")) {
            ConfigurationSection cs = api.hologramFile.getConfig().getConfigurationSection("Holograms");
            for (String key : cs.getKeys(false)) {
                List<String> list = api.hologramFile.getConfig().getStringList("Holograms." + key + ".lines");
                Location location = api.serialize().getInstance().unserializeLocation(api.hologramFile.getConfig().getString("Holograms." + key + ".location"));
                holograms.add(new HologramObject(key, location, new ArrayList<>(list)));
            }
        }
        return holograms;
    }

    public void saveHologram(HologramObject hologram) {
        if (hologram.getName() == null || hologram.getWorld() == null || hologram.getLocation() == null) return;
        String serial = api.serialize().serializeLocation(hologram.getLocation());
        api.hologramFile.getConfig().set("Holograms." + hologram.getName() + ".location", serial);
        api.hologramFile.getConfig().set("Holograms." + hologram.getName() + ".lines", hologram.getLines());
    }
}