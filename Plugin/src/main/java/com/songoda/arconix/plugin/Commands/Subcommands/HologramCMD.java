package com.songoda.arconix.plugin.Commands.Subcommands;

import com.songoda.arconix.api.ArconixAPI;
import com.songoda.arconix.api.hologram.HologramObject;
import com.songoda.arconix.api.methods.Formatting;
import com.songoda.arconix.plugin.Commands.SubCommand;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Kiran Hart on 4/4/2017.
 */
public class HologramCMD extends SubCommand {

    private Formatting formatting = new Formatting();

    public HologramCMD() {
        super("hologram", "hologram", "ArconixCMD.command.hologram", "Make a hologram", 4);
    }

    @Override
    public void execute(Player p, String[] args) {

        if (args.length == 1) {
            p.sendMessage(formatting.formatText("&e/Arconix hologram create/delete/movehere/addline/listlines/removeline"));
        }

        if (args.length == 2) {

            switch (args[1].toLowerCase()) {

                case "show":
                    //plugin.holo.handleMovement(p.getLocation(), null, p);
                    break;
                case "hide":
                    //arconix.packetLibrary.getHologramManager().hideHolograms(p);
                    break;
                case "movehere":
                    p.sendMessage(formatting.formatText("&e/Arconix hologram movehere [hologram]"));
                case "delete":
                    p.sendMessage(formatting.formatText("&e/Arconix hologram delete [hologram]"));
                    break;
                case "create":
                    p.sendMessage(formatting.formatText("&e/Arconix hologram create [hologram] [title] [text]"));
                    break;
                case "addline":
                    p.sendMessage(formatting.formatText("&e/Arconix hologram addline [hologram] [title] [text]"));
                    break;
                case "listlines":
                    p.sendMessage(formatting.formatText("&e/Arconix hologram listlines [hologram]"));
                    break;
                case "removeline":
                    p.sendMessage(formatting.formatText("&e/Arconix hologram removeline [hologram] [line]"));
                    break;
                default:
                    break;
            }
        }

        if (args.length >= 3) {
            switch (args[1].toLowerCase()) {
                case "movehere":
                    for (HologramObject hologram : ArconixAPI.getApi().packetLibrary.getHologramManager().getHolograms()) {
                        if (hologram.getName() != null && hologram.getName().equalsIgnoreCase(args[2].trim())) {
                            Location location = hologram.getLocation();
                            double increment = 0;
                            for (int i = 0; i < hologram.getLines().size(); i++) {
                                Location location2 = location.clone().subtract(0, increment, 0);
                                for (Entity entity : location.getWorld().getNearbyEntities(location2, .2, .2, .2)) {
                                    if (location2.getX() == entity.getLocation().getX() && location2.getY() == entity.getLocation().getY() && location2.getZ() == entity.getLocation().getZ()) {
                                        entity.teleport(p.getLocation().subtract(0, increment, 0));
                                    }
                                }
                                increment += .25;
                            }
                            hologram.setLocation(p.getLocation());
                        }

                    }
                    break;
                case "addline":
                    HologramObject hologram = ArconixAPI.getApi().packetLibrary.getHologramManager().getHologram(args[2].trim());
                    if (hologram == null) return;
                    ArrayList<String> lines = hologram.getLines();
                    Location location = hologram.getLocation();

                    StringBuilder text = new StringBuilder();
                    for (int i = 3; i < args.length; i++) {
                        String arg = args[i] + " ";
                        text.append(arg);
                    }
                    lines.add(formatting.formatText(text.toString().trim()));

                    ArconixAPI.getApi().packetLibrary.getHologramManager().removeHologram(hologram);

                    ArconixAPI.getApi().packetLibrary.getHologramManager().addHologram(new HologramObject(args[2].trim(), location, lines));
                    break;
                case "removeline":
                    HologramObject hologram222 = ArconixAPI.getApi().packetLibrary.getHologramManager().getHologram(args[2].trim());
                    if (hologram222 == null) return;
                    ArrayList<String> lines222 = hologram222.getLines();
                    Location location222 = hologram222.getLocation();

                    lines222.remove(Integer.parseInt(args[3]) - 1);

                    ArconixAPI.getApi().packetLibrary.getHologramManager().removeHologram(hologram222);

                    ArconixAPI.getApi().packetLibrary.getHologramManager().addHologram(new HologramObject(args[2].trim(), location222, lines222));
                    break;
                case "listlines":
                    HologramObject hologram22 = ArconixAPI.getApi().packetLibrary.getHologramManager().getHologram(args[2].trim());

                    int line2 = 1;

                    p.sendMessage(formatting.formatText("&7Showing lines for hologram: &6" + hologram22.getName() + "&7."));
                    for (String line : hologram22.getLines()) {
                        p.sendMessage(formatting.formatText("&6" + line2 + ": &7" + line));
                        line2++;
                    }

                    break;
                case "delete":
                    HologramObject hologram2 = ArconixAPI.getApi().packetLibrary.getHologramManager().getHologram(args[2].trim());
                    ArconixAPI.getApi().packetLibrary.getHologramManager().removeHologram(hologram2);

                    break;
                case "create":
                    if (args.length < 4) return;
                    StringBuilder text2 = new StringBuilder();
                    for (int i = 3; i < args.length; i++) {
                        String arg = args[i] + " ";
                        text2.append(arg);
                    }
                    HologramObject hologram3 = ArconixAPI.getApi().packetLibrary.getHologramManager().addHologram(new HologramObject(args[2].trim(), p.getLocation(), formatting.formatText(text2.toString().trim())));
                    break;
                default:
                    break;
            }
        }
    }
}
