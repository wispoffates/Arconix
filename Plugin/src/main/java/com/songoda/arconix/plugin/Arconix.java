package com.songoda.arconix.plugin;

import com.songoda.arconix.api.ArconixAPI;
import com.songoda.arconix.api.events.CustomEventListeners;
import com.songoda.arconix.api.events.RegionEvents;
import com.songoda.arconix.api.handlers.LocalHologramHandler;
import com.songoda.arconix.api.hologram.HologramObject;
import com.songoda.arconix.api.packets.PacketLibrary;
import com.songoda.arconix.api.tasks.HologramShowTask;
import com.songoda.arconix.plugin.Commands.ArconixCMD;
import com.songoda.arconix.plugin.Commands.BaseCommand;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.List;

/**
 * Created by songoda on 3/16/2017.
 */
public class Arconix extends JavaPlugin implements Listener {
    private String serverVersion = getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];

    private ArconixAPI api;

    public static Arconix pl() {
        return (Arconix) Bukkit.getServer().getPluginManager().getPlugin("Arconix");
    }

    @Override
    public void onEnable() {
        api = ArconixAPI.getApi().init(this);

        new HologramShowTask(api);

        HologramShowTask.startTask(api);

        setupConfig();
        api.packetLibrary = new PacketLibrary();
        api.packetLibrary.setupPackets(serverVersion);

        getServer().getPluginManager().registerEvents(new CustomEventListeners(), this);
        getServer().getPluginManager().registerEvents(new RegionEvents(), this);

        List<BaseCommand> commands = Collections.singletonList(new ArconixCMD());
        for (BaseCommand command : commands) {
            getCommand(command.getName()).setExecutor(command);
        }

        Bukkit.getScheduler().runTaskLater(this, () -> {
            for (HologramObject hologram : api.getHolo().getHologramList()) {
                api.packetLibrary.getHologramManager().addHologram(hologram);
            }
        }, 10);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::doSave, 6000, 6000);

    }

    private void doSave() {
        for (HologramObject hologram : api.packetLibrary.getHologramManager().getHolograms()) {
            if (hologram.getName() == null) continue;
            api.getHolo().saveHologram(hologram);
        }
        api.hologramFile.saveConfig();
    }

    @Override
    public void onDisable() {
        doSave();
    }

    public void reload() {
        api.reload();
        reloadConfig();
        saveConfig();
    }

    private void setupConfig() {

        getConfig().addDefault("settings.Countdown-format", "%d hour(s), %d min(s), %d sec(s)");
        getConfig().addDefault("settings.ECO-format", "#,###.00");
        getConfig().addDefault("settings.Hologram-Distance", 15);

        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    public ArconixAPI hook(JavaPlugin jp) {
        getLogger().info("Plugin hooked: " + jp.getName());
        return api;
    }

    public ArconixAPI getApi() {
        return api;
    }

    public LocalHologramHandler getHolo() {
        return api.getHolo();
    }

    public String getServerVersion() {
        return serverVersion;
    }
}