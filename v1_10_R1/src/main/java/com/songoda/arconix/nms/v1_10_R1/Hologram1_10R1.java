package com.songoda.arconix.nms.v1_10_R1;

import com.songoda.arconix.api.hologram.HologramObject;
import com.songoda.arconix.api.packets.Hologram;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.*;

@SuppressWarnings("Duplicates")
public class Hologram1_10R1 implements Hologram {

    private Map<Location, HologramObject> registeredHolograms = new HashMap<>();

    public Hologram1_10R1() {
        reloadHologram();
    }

    private void reloadHologram() {
        for (HologramObject hologram : registeredHolograms.values()) {
            addHologram(hologram);
        }
    }

    @Override
    public HologramObject addHologram(HologramObject hologram) {
        if (hologram == null || hologram.getWorld() == null || hologram.getLocation() == null) return null;
        this.registeredHolograms.put(hologram.getLocation(), hologram);

        double increment = 0;

        for (String line : hologram.getLines()) {
            Location location = hologram.getLocation().subtract(0, increment, 0);

            Collection<org.bukkit.entity.Entity> nearbyEntite = location.getWorld().getNearbyEntities(location, 1, 1, 1);
            nearbyEntite.removeIf(e -> e.getType() != EntityType.ARMOR_STAND || e.getLocation().getX() != location.getX() || e.getLocation().getY() != location.getY() || e.getLocation().getZ() != location.getZ());

            EntityArmorStand nmsEntity = new EntityArmorStand(((CraftWorld) hologram.getWorld()).getHandle(), hologram.getX(), hologram.getY() - increment, hologram.getZ());

            if (nearbyEntite.size() == 0) {
                removeEntityFromWorld(hologram, location);
            } else {
                nearbyEntite.iterator().next().setCustomName(line);
                increment += .25;
                continue;
            }

            nmsEntity.setCustomName(line); // Only difference between 1.12 and 1.13
            nmsEntity.setCustomNameVisible(true);
            nmsEntity.setInvisible(true);
            nmsEntity.setNoGravity(true);
            nmsEntity.setSmall(true);
            nmsEntity.setMarker(true);
            nmsEntity.setBasePlate(true);

            WorldServer nmsWorld = ((CraftWorld) hologram.getWorld()).getHandle();

            this.setPosition(nmsEntity, location);

            if (nearbyEntite.size() == 0) {
                nmsWorld.addEntity(nmsEntity, CreatureSpawnEvent.SpawnReason.CUSTOM);
            }

            increment += .25;
        }

        return hologram;
    }

    @Override
    public HologramObject removeHologram(HologramObject hologram) {
        registeredHolograms.remove(hologram.getLocation());
        removeHologram(hologram, hologram.getLocation(), hologram.getLines().size());
        return hologram;
    }

    @Override
    public void removeHologram(HologramObject hologramObject, Location location, int lines) {
        if (location == null) return;

        double increment = 0;

        for (int i = 0; i < lines; i++) {
            Location location2 = location.clone().subtract(0, increment, 0);

            removeEntityFromWorld(hologramObject, location2);

            increment += .25;
        }
    }

    @Override
    public void removeHologram(Location location, int lines) {
        removeHologram(null, location, lines);
    }

    @Override
    public void updateVisible(HologramObject hologramObject, Location location, Player player, boolean visible) {
        double increment = 0;

        boolean obNull = hologramObject == null;

        int lines = obNull ? 1 : hologramObject.getLineCount();

        boolean isvisible = !obNull && hologramObject.isVisible(player);

        for (int i = 0; i < lines; i++) {
            Location location2 = location.clone().subtract(0, increment, 0);
            increment += .25;
            for (org.bukkit.entity.Entity entity : getEntityFromWorld(location2)) {
                Packet<?> packet;
                if (visible) {
                    if (!obNull && isvisible) continue;
                    packet = new PacketPlayOutSpawnEntityLiving((EntityLiving) ((CraftEntity) entity).getHandle());
                    if (!obNull) hologramObject.setVisible(player, true);
                } else {
                    if (!obNull && !isvisible) continue;
                    packet = new PacketPlayOutEntityDestroy(entity.getEntityId());
                    if (!obNull) hologramObject.setVisible(player, false);
                }
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
            }
        }
    }

    private void setPosition(Entity entity, Location location) {
        entity.setPosition(location.getX(), location.getY(), location.getZ());
        PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(entity);
        for (EntityHuman eh : entity.world.players) {
            if (!(eh instanceof EntityPlayer)) continue;
            EntityPlayer player = (EntityPlayer) eh;
            double distanceSquared = Math.pow(player.locX - entity.locX, 2) + Math.pow(player.locZ - entity.locZ, 2);
            if (distanceSquared < 8192 && player.playerConnection != null) {
                player.playerConnection.sendPacket(teleportPacket);
            }

        }
    }

    private void removeEntityFromWorld(HologramObject hologramObject, Location location) {
        Collection<org.bukkit.entity.Entity> entities = getEntityFromWorld(location);
        if (entities.size() == 0) return;
        for (org.bukkit.entity.Entity entity : entities) {
            entity.remove();
            registeredHolograms.remove(location);
            //for (Player player : Bukkit.getOnlinePlayers()) updateVisible(hologramObject, location, player, false);
        }
    }

    private Collection<org.bukkit.entity.Entity> getEntityFromWorld(Location location) {
        Collection<org.bukkit.entity.Entity> near = location.getWorld().getNearbyEntities(location, 2, 2, 2);
        near.removeIf(e -> e.getType() != EntityType.ARMOR_STAND || e.getLocation().getX() != location.getX() || e.getLocation().getY() != location.getY() || e.getLocation().getZ() != location.getZ());
        return near;
    }

    public HologramObject getHologram(Location location) {
        return registeredHolograms.get(location);
    }

    @Override
    public HologramObject getHologram(String name) {
        for (HologramObject hologram : registeredHolograms.values()) {
            if (hologram.getName() != null && hologram.getName().equalsIgnoreCase(name))
                return hologram;
        }
        return null;
    }


    @Deprecated
    public void spawnHolograms(Location location, List<String> holograms) {
        addHologram(new HologramObject(null, location, new ArrayList<>(holograms)));
    }

    @Deprecated
    public void spawnHologram(Location location, String line) {
        addHologram(new HologramObject(null, location, line));
    }

    @Deprecated
    public void despawnHologram(Location location) {
        removeHologram(location, 5);
    }

    @Override
    public void addHologram(Location location) {
        addHologram(new HologramObject(null, location, (String) null));

    }

    @Deprecated
    public ArrayList<Location> getLocations() {
        ArrayList<Location> locations = new ArrayList<>();
        for (HologramObject hologram : registeredHolograms.values()) {
            locations.add(hologram.getLocation());
        }
        return locations;
    }

    @Override
    public List<HologramObject> getHolograms() {
        return new ArrayList<>(registeredHolograms.values());
    }
}
